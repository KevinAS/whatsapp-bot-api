from django.shortcuts import render
from django.http import JsonResponse, HttpResponseBadRequest
from selenium.common.exceptions import TimeoutException
import json
import traceback
import os
import shutil

from wa_bot import models
from . import wa_core
# Create your views here.

all_bot_drivers = dict()

def create_bot_instance(request):
    if request.method=="POST":
        print("creating bot instance...")
        bot_driver_id = len(all_bot_drivers)
        proxy_port = 9780+bot_driver_id
        print("bot_id", bot_driver_id, "proxy_port", proxy_port)
        
        user_data_dir = f"udd{str(bot_driver_id)}"
        if os.path.isdir(user_data_dir):
            shutil.rmtree(user_data_dir)

        driver = wa_core.initialize_chrome(headless=False, user_data_dir=user_data_dir, port_num=proxy_port)
        driver.get("https://web.whatsapp.com/")

        qr = None
        try:
            qr = wa_core.get_qrcode(driver)    
        except TimeoutException as te0:
            print("Failed to create bot instance: get_qrcode() timeout")
            print(te0)
            traceback.print_exc()
            return JsonResponse({"status": 1, "msg": traceback.format_exc()})

        all_bot_drivers[bot_driver_id] = driver
        json_response = {"status": 0, "bot_id": bot_driver_id}
        if qr != None:
            json_response["qr"] = qr[0]
        
        return JsonResponse(json_response)

def send_msg(request):
    if request.method=="POST":
        request_json = json.loads(request.body)
        try:
            bot_id = int(request_json["bot_id"])
            messages = request_json["messages"]
            by = int(request_json["by"])
            if by==0:
                #by providing list of numbers
                number_list = request_json["number_list"]
            elif by==1:
                #by groud id in database
                group_id = request_json["group_id"]
            else:
                return HttpResponseBadRequest("")

            bot_driver = all_bot_drivers[bot_id]
        except:
            traceback.print_exc()
            print(all_bot_drivers)
            return HttpResponseBadRequest("")
        
        failed_numbers = []

        if by==1:
            number_list = models.Contacts.objects.filter(group_id=group_id)
            
        for number in number_list:
            try:
                wa_core.go_to_chat(bot_driver, number)
                wa_core.send_msg(bot_driver, bot_msg="hello", quit_chat=False)
            except:
                failed_numbers.append(number)

        wa_core.go_to_chat(bot_driver, "Self")
    
        return JsonResponse({"failed_numbers": failed_numbers})