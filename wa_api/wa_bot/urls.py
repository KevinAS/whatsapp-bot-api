from django.urls import path
from django.urls import include
from django.shortcuts import render, redirect
from django.contrib.auth import logout
from . import views

app_name='wa_bot'
urlpatterns = [
    path('send_msg/', views.send_msg, name='send_msg'),
    path('create_bot_instance/', views.create_bot_instance, name='create_bot_instance')   
]