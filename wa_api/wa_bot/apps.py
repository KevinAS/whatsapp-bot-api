from django.apps import AppConfig


class WaBotConfig(AppConfig):
    name = 'wa_bot'
